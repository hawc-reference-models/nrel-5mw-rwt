# -*- coding: utf-8 -*-
"""Prepare the zip file for the model
"""
from refutils.utils import ReferenceModel


class NREL5MWRWT(ReferenceModel):
    def __init__(self):
        model_kwargs = dict(pitch_f=100, pitch_z=0.7, cut_in=4, cut_out=25, n_wsp=22,
                            gen_min=670, gen_max=1173.7, gbr=97, pitch_min=0, opt_lambda=7.55,
                            rate_pow=5000, gen_eff=0.944, p1_f=0.05, p1_z=0.7, p2_f=0.10, p2_z=0.7,
                            gs=1, constant_power=0, dt=40, tstart=100, wsp=24,
                            tb_wid=130, tb_ht=130) # rotor diameter is 126 m
        model_kwargs['tint'] = 0.14*(0.75*model_kwargs['wsp'] + 5.6) / model_kwargs['wsp']  # class 1B
        dll_list = [('damper_5mw.dll', 'damper.dll'),
                    ('hss_convert_5mw.dll', 'hss_convert.dll'),
                    ('DISCON_NREL5MW_v0.2.0.dll', 'DISCON.dll'),
                    ('nrel_5mw_interface_v0.2.0.dll', 'nrel_5mw_interface.dll')]
        ReferenceModel.__init__(self,
                                model_path_name='nrel-5mw-rwt',
                                htc_basename='nrel_5mw_rwt',
                                model_kwargs=model_kwargs,
                                dll_list=dll_list,
                                step=True, turb=True, hs2=True)


if __name__ == '__main__':
    model = NREL5MWRWT()
    model.prepare_model()
    #model.test_with_hawc2binary_master()
